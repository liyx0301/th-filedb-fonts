import request from '@/utils/request';


export const getRoute = (): Promise<string[]> =>
  request.get<string[]>('/api/global/routes').catch((err) => []);
  
