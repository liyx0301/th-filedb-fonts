import { SortOrder } from 'antd/lib/table/interface';


/** 表格通用请求参数类型 */
export interface TableCommonParams<A = any, T extends string = string> {
  /** 业务中需要的其它字段 */
  fields?: A;
  params: {
    /** 每页的条目 */
    pageSize?: number;
    /** 当前请求页数 */
    current?: number;
    /** 搜索词 */
    keyword?: string;
  };
  /** 排序字段 */
  sort?: Record<T, SortOrder>;
  /** 过滤字段 */
  filter?: Record<T, (string | number | null)[]>;
}

export type ACL = 'PRIVATE' | 'PUBLIC_READ' | 'PUBLIC_READ_WRITE';

export const ACL_Mapping = {
  PRIVATE: '私有',
  PUBLIC_READ: '公共读',
  PUBLIC_READ_WRITE: '公共读写',
};