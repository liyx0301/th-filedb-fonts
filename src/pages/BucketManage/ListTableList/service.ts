import request from '@/utils/request';
import { TableCommonParams } from '@/common-types/common';

export interface FileItem {
  id: string;
  title: string;
  state: 'Error' | 'Success' | 'Processing';
  labels: { name: string; color: string; }[];
  created_time: string;
}

export const getFileList = (params: TableCommonParams) =>
  request.get<FileItem[]>('/api/bucket_manage/file_manage', { params });

export interface UploadFile {
  name: string;
  url: string;
  owner: string; // 用户id
  type: 'private' | 'public';
  approver: string; // 用户id
  startTime: string;
  endTime: string;
  description: string;
}

/** 上传 */
export const uploadFile = (data: UploadFile) => request.post('/api/bucket_manage/upload_file', { data });  

export interface FolderItem {
  name: string;
  url: string;
  owner: string; // 用户id
  type: 'private' | 'public';
  approver: string; // 用户id
  startTime: string;
  endTime: string;
  description: string;
}

/** 添加目录 */
export const addFolder = (data: FolderItem) => request.post('/api/bucket_manage/add_folder', { data });


/** 下载/删除 */
export const operateFiles = (ids: string[], type: 'delete' | 'download') =>
  request.post('/api/bucket_manage/files_operate', { data: { type, ids } });



// export async function removeRule(params: { key: number[] }) {
//   return request('/api/rule', {
//     method: 'POST',
//     data: {
//       ...params,
//       method: 'delete',
//     },
//   });
// }

// export async function addRule(params: TableListParams) {
//   return request('/api/rule', {
//     method: 'POST',
//     data: {
//       ...params,
//       method: 'post',
//     },
//   });
// }

// export async function updateRule(params: TableListParams) {
//   return request('/api/rule', {
//     method: 'POST',
//     data: {
//       ...params,
//       method: 'update',
//     },
//   });
// }
