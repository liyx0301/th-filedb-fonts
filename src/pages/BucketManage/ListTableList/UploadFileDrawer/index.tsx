import React from "react";
import styles from "./index.less";
import { Moment } from 'moment';
import {
  Drawer,
  Form,
  Button,
  Col,
  Row,
  Input,
  Select,
  DatePicker,
  message
} from "antd";
import { FormInstance } from 'antd/lib/form';
import { PlusOutlined } from "@ant-design/icons";
import { UploadFile, uploadFile } from '../service';

const { Option } = Select;



class DrawerForm extends React.Component {
  state = { visible: false, loading: false, };

  formRef = React.createRef<FormInstance>();

  showDrawer = () => {
    this.setState({
      visible: true
    });
  };

  onClose = () => {
    this.setState({
      visible: false
    });
  };

  onFinish = (values: Omit<UploadFile, 'startTime' | 'endTime'> & { datetime: [Moment, Moment] }) => {
    this.setState({ loading: true });
    const params = {
      ...values,
      datetime: undefined,
      startTime: values.datetime[0].format('YYYY-MM-DD'),
      endTime: values.datetime[1].format('YYYY-MM-DD'),
    };
    uploadFile(params).then(() => {
      message.success('上传成功');
      this.setState({ visible: false });
    }).catch((err) => {
      message.error(err.message);
    }).finally(() => {
      this.setState({ loading: false });
    });
  };

  render() {
    return (
      <div>
        <Button type="primary" onClick={this.showDrawer}>
          <PlusOutlined /> 上传
        </Button>
        <Drawer
          title="上传"
          width={720}
          onClose={this.onClose}
          visible={this.state.visible}
          bodyStyle={{ paddingBottom: 80 }}
          footer={
            <div
              style={{
                textAlign: "right"
              }}
            >
              <Button onClick={this.onClose} style={{ marginRight: 8 }}>
                Cancel
              </Button>
              <Button onClick={() => {this.formRef.current?.submit()}} type="primary">
                Submit
              </Button>
            </div>
          }
        >
          {/* TODO : 问题 */}
          <Form layout="vertical" ref={this.formRef} hideRequiredMark onFinish={this.onFinish}>
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item
                  name="name"
                  label="Name"
                  rules={[
                    { required: true, message: "Please enter user name" }
                  ]}
                >
                  <Input placeholder="Please enter user name" />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="url"
                  label="Url"
                  rules={[{ required: true, message: "Please enter url" }]}
                >
                  <Input
                    style={{ width: "100%" }}
                    addonBefore="http://"
                    addonAfter=".com"
                    placeholder="Please enter url"
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item
                  name="owner"
                  label="Owner"
                  rules={[
                    { required: true, message: "Please select an owner" }
                  ]}
                >
                  <Select placeholder="Please select an owner">
                    <Option value="xiao">Xiaoxiao Fu</Option>
                    <Option value="mao">Maomao Zhou</Option>
                  </Select>
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="type"
                  label="Type"
                  rules={[
                    { required: true, message: "Please choose the type" }
                  ]}
                >
                  <Select placeholder="Please choose the type">
                    <Option value="private">私有</Option>
                    <Option value="public">公开</Option>
                  </Select>
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item
                  name="approver"
                  label="Approver"
                  rules={[
                    { required: true, message: "Please choose the approver" }
                  ]}
                >
                  <Select placeholder="Please choose the approver">
                    <Option value="jack">Jack Ma</Option>
                    <Option value="tom">Tom Liu</Option>
                  </Select>
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="dateTime"
                  label="DateTime"
                  rules={[
                    { required: true, message: "Please choose the dateTime" }
                  ]}
                >
                  <DatePicker.RangePicker
                    style={{ width: "100%" }}
                    getPopupContainer={trigger => trigger.parentElement || document.body}
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={24}>
                <Form.Item
                  name="description"
                  label="Description"
                  rules={[
                    {
                      required: true,
                      message: "please enter url description"
                    }
                  ]}
                >
                  <Input.TextArea
                    rows={4}
                    placeholder="please enter url description"
                  />
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </Drawer>
      </div>
    );
  }
}

export default () => (
  <div className={styles.container}>
    <div id="components-drawer-demo-form-in-drawer">
      <DrawerForm />
    </div>
  </div>
);
