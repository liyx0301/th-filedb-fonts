import React from "react";
import {
  Drawer,
  Form,
  Button,
  Col,
  Row,
  Input,
  Select,
  DatePicker,
  message,
} from "antd";
import { Moment } from 'moment';
import { PlusOutlined } from "@ant-design/icons";
import { FormInstance } from 'antd/lib/form';
import { FolderItem, addFolder } from '../service';

import styles from "./index.less";

const { Option } = Select;

class DrawerForm extends React.Component {
  formRef = React.createRef<FormInstance>();

  state = { visible: false,  };

  showDrawer = () => {
    this.setState({
      visible: true
    });
  };

  onClose = () => {
    this.setState({
      visible: false
    });
  };

  createBucketSubmit = (values: Omit<FolderItem, 'startTime' | 'endTime'> & { dateTime: [Moment, Moment] }) => {
    this.setState({
      loading: true,
    });
    
    const params = {
      ...values,
      dateTime: undefined,
      startTime: values.dateTime[0].format('YYYY-MM-DD'),
      endTime: values.dateTime[1].format('YYYY-MM-DD'),
    };
    addFolder(params)
      .then(() => {
        message.success('添加目录成功');
        this.setState({ visible: false });
        this.formRef.current?.resetFields(['dateTime', 'name', 'url', 'owner', 'type', 'approver', 'description']);
      })
      .catch((err) => {
        message.error(err.message);
      })
      .finally(() => {
        this.setState({
          loading: false,
        });
      });
  };

  render() {
    return (
      <div>
        <Button type="primary" onClick={this.showDrawer}>
          <PlusOutlined /> 新建目录
        </Button>
        <Drawer
          title="新建目录"
          width={720}
          onClose={this.onClose}
          visible={this.state.visible}
          bodyStyle={{ paddingBottom: 80 }}
          footer={
            <div
              style={{
                textAlign: "right"
              }}
            >
              <Button onClick={this.onClose} style={{ marginRight: 8 }}>
                Cancel
              </Button>
              <Button onClick={() => { this.formRef.current?.submit() }} type="primary">
                Submit
              </Button>
            </div>
          }
        >
          <Form layout="vertical" ref={this.formRef} hideRequiredMark onFinish={this.createBucketSubmit}>
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item
                  name="name"
                  label="Name"
                  rules={[
                    { required: true, message: "Please enter user name" }
                  ]}
                >
                  <Input placeholder="Please enter user name" />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="url"
                  label="Url"
                  rules={[{ required: true, message: "Please enter url" }]}
                >
                  <Input
                    style={{ width: "100%" }}
                    addonBefore="http://"
                    addonAfter=".com"
                    placeholder="Please enter url"
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item
                  name="owner"
                  label="Owner"
                  rules={[
                    { required: true, message: "Please select an owner" }
                  ]}
                >
                  <Select placeholder="Please select an owner">
                    <Option value="xiao">Xiaoxiao Fu</Option>
                    <Option value="mao">Maomao Zhou</Option>
                  </Select>
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="type"
                  label="Type"
                  rules={[
                    { required: true, message: "Please choose the type" }
                  ]}
                >
                  <Select placeholder="Please choose the type">
                    <Option value="private">Private</Option>
                    <Option value="public">Public</Option>
                  </Select>
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item
                  name="approver"
                  label="Approver"
                  rules={[
                    { required: true, message: "Please choose the approver" }
                  ]}
                >
                  <Select placeholder="Please choose the approver">
                    <Option value="jack">Jack Ma</Option>
                    <Option value="tom">Tom Liu</Option>
                  </Select>
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="dateTime"
                  label="DateTime"
                  rules={[
                    { required: true, message: "Please choose the dateTime" }
                  ]}
                >
                  <DatePicker.RangePicker
                    style={{ width: "100%" }}
                    getPopupContainer={trigger => trigger.parentElement || document.body}
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={24}>
                <Form.Item
                  name="description"
                  label="Description"
                  rules={[
                    {
                      required: true,
                      message: "please enter url description"
                    }
                  ]}
                >
                  <Input.TextArea
                    rows={4}
                    placeholder="please enter url description"
                  />
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </Drawer>
      </div>
    );
  }
}

export default () => (
  <div className={styles.container}>
    <div id="components-drawer-demo-form-in-drawer">
      <DrawerForm />
    </div>
  </div>
);
