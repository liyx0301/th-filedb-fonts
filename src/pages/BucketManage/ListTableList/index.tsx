import React, { useRef, useState } from 'react';
import { PlusOutlined, EllipsisOutlined } from '@ant-design/icons';
import { Button, Tag, Space, Menu, Dropdown, Table, message } from 'antd';
import type { ProColumns, ActionType } from '@ant-design/pro-table';
import ProTable, { TableDropdown } from '@ant-design/pro-table';
import AddFolderDrawer from './AddFolderDrawer';
import SetObjectAclDrawer from './SetObjectAclDrawer';
import UploadFileDrawer from './UploadFileDrawer';

import { FileItem, getFileList, operateFiles } from './service';

const statusMapping = {
  'Error': '未解决',
  Success: '已解决',
  Processing: '解决中',
};

const columns: ProColumns<FileItem>[] = [
  {
    title: '标题',
    dataIndex: 'title',
    // copyable: true,
    ellipsis: true,
    width: '30%',
  },
  {
    title: '状态',
    dataIndex: 'state',
    initialValue: 'open',
    filters: true,
    onFilter: true,
    valueType: 'text',
    valueEnum: {
      Error: {
        text: '未解决',
      },
      Success: {
        text: '已解决',
      },
      Processing: {
        text: '解决中',
      },
    },
    render: (text, row) => statusMapping[row.state],
  },
  {
    title: '标签',
    dataIndex: 'labels',
    renderFormItem: (_, { defaultRender }) => {
      return defaultRender(_);
    },
    render: (_, record) => (
      <Space>
        {record.labels.map(({ name, color }) => (
          <Tag color={color} key={name}>
            {name}
          </Tag>
        ))}
      </Space>
    ),
  },
  {
    title: '创建时间',
    dataIndex: 'created_time',
    valueType: 'date',
  },
  {
    title: '操作',
    valueType: 'option',
    render: (text, row, _, action) => [
      <a key="view">
        详情
      </a>,
      <a key="view">
        下载
      </a>,
      <a key="view">
        设置权限
      </a>,
      
      <TableDropdown
        key="actionGroup"
        onSelect={() => action.reload()}
        menus={[
          { key: 'copyUrl', name: '复制URL' },
          { key: 'delete', name: '删除' },
        ]}
      />,
    ],
  },
];



const FileTable = () => {
  const actionRef = useRef<ActionType>();
  const [rowKeys, setRowKeys] = useState<string[]>([]);


  const batchOperate = (info: any) => {
    operateFiles(rowKeys, info.key).then(() => {
      // TODO :
      message.success('成功');
    }).catch((err) => {
      message.error(err.message);
    });
  };

  const menu = (
    <Menu onClick={batchOperate}>
      <Menu.Item key="delete">删除</Menu.Item>
      <Menu.Item key="download">下载</Menu.Item>
    </Menu>
  );

  return (
    <ProTable<FileItem>
      columns={columns}
      actionRef={actionRef}
      request={(params, sort, filter) => getFileList({ params, sort, filter })}
      // bordered={true}
      rowKey="id"
      tableAlertRender={false}
      rowSelection={{
        selectedRowKeys: rowKeys,
        onChange: setRowKeys as any,
      }}
      search={false}
      options={{
        search: true,
      }}
      pagination={{
        pageSize: 5,
      }}
      // dateFormatter="string"
      headerTitle={
        <Space>
          <UploadFileDrawer />
          <AddFolderDrawer />
          <SetObjectAclDrawer />
          <Dropdown key="menu" overlay={menu} disabled={rowKeys.length === 0}>
            <Button>
              批量操作
            </Button>
          </Dropdown>
        </Space>
      }
    />
  );
};

export default FileTable;
