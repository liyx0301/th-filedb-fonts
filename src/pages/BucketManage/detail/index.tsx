import React from 'react';
import { Descriptions } from 'antd';
import { useRequest } from 'ahooks';
import { Line } from '@ant-design/charts';
import ProCard from '@ant-design/pro-Card';
import { getDetailBasic, getDetailChart } from '../services';


const config = {
  xField: 'date',
  yField: 'num',
  autoFit: false,
  point: {
    size: 5,
    shape: 'diamond',
    style: {
      fill: 'white',
      stroke: '#5B8FF9',
      lineWidth: 2,
    },
  },
  tooltip: {
    formatter: (datum: DataCue) => {
      return {
        name: '新增：',
        value: `${datum.num}个`,
      };
    },
  },
  state: {
    active: {
      style: {
        shadowColor: 'yellow',
        shadowBlur: 4,
        stroke: 'transparent',
        fill: 'red',
      },
    },
  },
  theme: {
    geometries: {
      point: {
        diamond: {
          active: {
            style: {
              shadowColor: '#FCEBB9',
              shadowBlur: 2,
              stroke: '#F6BD16',
            },
          },
        },
      },
    },
  },
  interactions: [
    {
      type: 'marker-active',
    },
  ],
};

const Detail = () => {

  const basic = useRequest(() => getDetailBasic());
  const chartData = useRequest(() => getDetailChart());
  const basicData = basic.data;

  return (
    <>
      <ProCard title="基础信息" loading={basic.loading}>
        <Descriptions>
          <Descriptions.Item label="创建时间">{basicData?.create_time}</Descriptions.Item>
          <Descriptions.Item label="文件量">{basicData?.file_count}</Descriptions.Item>
          <Descriptions.Item label="大小">{basicData?.size_count}</Descriptions.Item>
          <Descriptions.Item label="ACL控制">{basicData?.prevent_theft_url}</Descriptions.Item>
          <Descriptions.Item label="防盗链接">未开启</Descriptions.Item>
        </Descriptions>
      </ProCard>
      <ProCard title="最近30天的对象引用次数统计" loading={chartData.loading}>
        <Line height={300} {...config} data={chartData.data?.quote || []} />
      </ProCard>
      <ProCard title="最近30天的新增对象数统计" loading={chartData.loading}>
        <Line height={300} {...config} data={chartData.data?.add || []} />
      </ProCard>
    </>
  );
};

export default React.memo(Detail);
