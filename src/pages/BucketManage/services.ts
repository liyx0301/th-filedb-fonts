import request from '@/utils/request';
import { ACL } from '@/common-types/common';

interface DetailBasic {
  create_time: string;
  file_count: number;
  size_count: number;
  acl: ACL;
  prevent_theft_url: string;
}

/** 概览基本数据 */
export const getDetailBasic = () => request.get<DetailBasic>('/api/bucket_manage/datail_basic');

export interface DetailChartItem {
  date: string;
  num: number;
}

export type DetailChartType = 'add' | 'quote';

/** 概览图表 */
export const getDetailChart = () =>
  request.get<Record<DetailChartType, DetailChartItem[]>>('/api/bucket_manage/datail_chart');

/** 获取热点数据 */
export const getHotData = () => request.get<DetailChartItem[]>('/api/bucket_manage/hot_data');

