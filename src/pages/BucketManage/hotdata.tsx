import React from 'react';
import { useRequest } from 'ahooks';
import ProCard from '@ant-design/pro-Card';
import { Line } from '@ant-design/charts';
import { getHotData } from './services';

const config = {
  xField: 'date',
  yField: 'num',
  autoFit: false,
  point: {
    size: 5,
    shape: 'diamond',
    style: {
      fill: 'white',
      stroke: '#5B8FF9',
      lineWidth: 2,
    },
  },
  tooltip: {
    formatter: (datum: DataCue) => {
      return {
        name: '新增：',
        value: `${datum.num}个`,
      };
    },
  },
  state: {
    active: {
      style: {
        shadowColor: 'yellow',
        shadowBlur: 4,
        stroke: 'transparent',
        fill: 'red',
      },
    },
  },
  theme: {
    geometries: {
      point: {
        diamond: {
          active: {
            style: {
              shadowColor: '#FCEBB9',
              shadowBlur: 2,
              stroke: '#F6BD16',
            },
          },
        },
      },
    },
  },
  interactions: [
    {
      type: 'marker-active',
    },
  ],
};

const HotData = () => {
  const { data, loading } = useRequest(() => getHotData());
  return (
    <ProCard title="热点数据" loading={loading}>
      <Line {...config} data={data || []} />
    </ProCard>
  );
};

export default React.memo(HotData);
