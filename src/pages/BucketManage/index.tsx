import { PageContainer } from '@ant-design/pro-layout';
import React, { Component } from 'react'
import ListTableList from './ListTableList';
import ProfileBasic from './ProfileBasic';
import Detail from './detail';
import HotData from './hotdata';


const contentList = {
  detail: Detail,
  fileManage: ListTableList,
  baseSetting: ProfileBasic,
  hotData: HotData,
};

class BucketManage extends Component {
  public state = {
    tabActiveKey: 'detail',
  };
  onTabChange = (tabActiveKey: string) => {
    this.setState({
      tabActiveKey,
    });
  };

  render() {
    const { tabActiveKey } = this.state;
    const Component = contentList[tabActiveKey];

    return (
      <PageContainer
        tabActiveKey={tabActiveKey}
        onTabChange={this.onTabChange}
        tabList={[
          {
            key: 'detail',
            tab: '概览',
          },
          {
            key: 'fileManage',
            tab: '文件管理',
          },
          {
            key: 'baseSetting',
            tab: '基础设置',
          },
          {
            key: 'hotData',
            tab: '热点数据',
          },
        ]}
      >
        <Component />
      </PageContainer>
    );
  }
}

export default BucketManage;
