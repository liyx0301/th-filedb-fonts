import request from '@/utils/request';
import { ACL } from '@/common-types/common';

export async function queryBasicProfile() {
  return request('/api/profile/basic');
}

export const getBucketApi = () => request.get('/api/bucket_manage/detail');

/** 设置 bucket 权限 */
export const setBucketAcl = (acl: ACL, bucket: string) =>
  request.post('/api/bucket_manage/set_acl', { data: { acl, bucket } });

export interface Referer {
  id: string;
  bucket: string;
  allowEmpty: boolean;
  whiteList: string;
}

/**  */
export const getBucketRefererApi = (params: { bucket: string }) =>
  request.get('/api/bucket_manage/referer', { params });

/** 设置 防盗链 */
export const updateBucketreferer = (data: Referer) =>
  request.post('/api/bucket_manage/update_referer', { data });

/** 删除 bucket */
export const deleteBucket = (data: { bucket: string }) =>
  request.post('/api/bucket_manage/delete_bucket', { data });
  // update_referer
// export const 
