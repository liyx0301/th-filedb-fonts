import React from "react";
import styles from "./index.less";
import {
  Drawer,
  Form,
  Button,
  Radio,
  Row,
  Input,
  Select,
  DatePicker
} from "antd";
import { PlusOutlined } from "@ant-design/icons";

const { Option } = Select;
const ACLMessageTable = {
  PRIVATE:
  <span style={{ fontSize: '12px' }}>私有：对文件的所有访问操作需要进行身份验证。</span>,
  PUBLIC_READ:
  <span style={{ color: 'red', fontSize: '12px' }}>公共读：对文件写操作需要进行身份验证；可以对文件进行匿名读。</span>,
  PUBLIC_READ_WRITE:
  <span style={{ color: 'red', fontSize: '12px' }}>公共读写：所有人都可以对文件进行读写操作。</span>,
};
const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    s: { span: 5 },
    sm: { span: 4 },
  },
  wrapperCol: {
    xs: { span: 24 },
    s: { span: 19 },
    sm: { span: 20 },
  },
};


class DrawerForm extends React.Component {

  state = { 
    visible: false,
    acl: 'PRIVATE',
    name: '',
    submitLoading: false,
    aclMessage: ACLMessageTable.PRIVATE,
    region: 'oss-shanghai-1', 
  };

  showDrawer = () => {
    this.setState({
      visible: true
    });
  };

  onClose = () => {
    this.setState({
      visible: false
    });
  };

  createBucketSubmit = (values) => {
    this.setState({
      submitLoading: true,
    });
    this.setState({
      submitLoading: false,
    });
  };

  aclChange = (e) => {
    e.preventDefault();
    this.setState({
      aclMessage: ACLMessageTable[e.target.value],
    });
  };

  checkBucketName = (rule, value) => {
    if (!value || value.trim().length < 3 || value.trim().length > 63) {
      return Promise.reject(<span style={{ fontSize: '12px' }}>请输入 Bucket 名称，3-63个字符长度</span>);
    }
    if (!/^[a-z0-9][a-z0-9-]{1,61}[a-z0-9]$/.test(value)) {
      return Promise.reject(<span style={{ fontSize: '12px' }}>只允许小写字母、数字、中划线（-），且不能以短横线开头或结尾</span>);
    }
    this.setState({
      name: value,
    });
    return Promise.resolve();
  };
  render() {
    const { name, acl, submitLoading, aclMessage, region } = this.state;
    return (
      <div>
        <Button type="primary" onClick={this.showDrawer}>
          <PlusOutlined /> 添加
        </Button>
        <Drawer
          title="添加 Server"
          width={640}
          onClose={this.onClose}
          visible={this.state.visible}
          bodyStyle={{ paddingBottom: 80 }}
          footer={
            <div
              style={{
                textAlign: "right"
              }}
            >
              <Button onClick={this.onClose} style={{ marginRight: 8 }}>
                取消
              </Button>
              <Button
              loading={submitLoading}
              type="primary"
              htmlType="submit"
              style={{
                marginRight: 8,
              }}
              disabled={!name}
            >
              确认
            </Button>
            </div>
          }
        >
          <Form{...formItemLayout} onFinish={this.createBucketSubmit} hideRequiredMark initialValues={{ name, acl, region }}>
            
                <Form.Item name="name"
                  label="Server 名称"
                  extra={<span style={{ fontSize: '12px' }}>Bucket 名称，3-63 个字符</span>}
                  rules={[{ required: true },
                    { validator: this.checkBucketName }]}
                >
                  <Input placeholder="Bucket" suffix={`${name.length}/63`} />
                </Form.Item>
                <Form.Item name="ip"
                  label="IP地址"
                  extra={<span style={{ fontSize: '12px' }}>Bucket 名称，3-63 个字符</span>}
                  rules={[{ required: true },
                    { validator: this.checkBucketName }]}
                >
                  <Input placeholder="Bucket" suffix={`${name.length}/63`} />
                </Form.Item>
                <Form.Item name="port"
                  label="端口"
                  extra={<span style={{ fontSize: '12px' }}>Bucket 名称，3-63 个字符</span>}
                  rules={[{ required: true },
                    { validator: this.checkBucketName }]}
                >
                  <Input placeholder="Bucket" suffix={`${name.length}/63`} />
                </Form.Item>
                <Form.Item name="strore"
                  label="存储位置"
                  extra={<span style={{ fontSize: '12px' }}>Bucket 名称，3-63 个字符</span>}
                  rules={[{ required: true },
                    { validator: this.checkBucketName }]}
                >
                  <Input placeholder="Bucket" suffix={`${name.length}/63`} />
                </Form.Item>
                <Form.Item
                  name="acl"
                  label="存储类型"
                  validateStatus="success"
                  help={aclMessage}
                  rules={[{
                    required: true,
                    message: '请选择存储类型!',
                  }]}
                >
                  <Radio.Group onChange={this.aclChange}>
                    <Radio.Button value="PRIVATE">主存储</Radio.Button>
                    <Radio.Button value="PUBLIC_READ">备存储</Radio.Button>
                    <Radio.Button value="PUBLIC_READ_WRITE">归档存储</Radio.Button>
                  </Radio.Group>
                </Form.Item>
                <Form.Item
                  name="region"
                  label="存储类型"
                  validateStatus="success"
                  rules={[{
                    required: true,
                    message: '请选择所属区域!',
                  }]}
                >
                  <Select placeholder="请选择所属区域">
                    <Option value="oss-shanghai-1">主存储服务</Option>
                  </Select>
                </Form.Item>

                
          </Form>
        </Drawer>
      </div>
    );
  }
}

export default () => (
  <div className={styles.container}>
    <div id="components-drawer-demo-form-in-drawer">
      <DrawerForm />
    </div>
  </div>
);
