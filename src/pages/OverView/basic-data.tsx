
import React, { useEffect } from 'react';
import { Alert, Statistic, message } from 'antd';
import ProCard from '@ant-design/pro-Card';
import { useRequest } from 'ahooks';
import { IntlShape } from 'react-intl';
import { getBasicData } from './service';

const { Divider } = ProCard;

interface Props {
  intl: IntlShape;
}

const BasicData = (props: Props) => {
  const { data, loading, error }= useRequest(() => getBasicData());

  useEffect(() => {
    if (error !== undefined) message.error(error.message);
  }, [error]);

  return (
    <>
      <Alert
        message={props.intl.formatMessage({
          id: '统计 基础 合计数据，平均延迟 1~3 小时。',
          defaultMessage: '统计 基础 合计数据，平均延迟 1~3 小时。',
        })}
        type="success"
        showIcon
        banner
        style={{
          margin: -20,
          marginBottom: 1,
        }}
      />
      <ProCard.Group className="ant-pro-card-contain-card" loading={loading}>
        {data?.map((i, index) => (
          <React.Fragment key={i.title}>
            <ProCard>
              <Statistic title={i.title} value={i.value}  precision={i.precision} suffix={i.suffix} />
            </ProCard>
            {data.length - 1 > index && <Divider />}
          </React.Fragment>
        ))}
      </ProCard.Group>
    </>
  );
};

export default BasicData;
