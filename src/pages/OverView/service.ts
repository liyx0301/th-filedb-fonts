
import request from '@/utils/request';
import { TableCommonParams, ACL } from '@/common-types/common';

export interface BasicDataItem {
  title: string;
  value: number;
  precision?: number;
  suffix?: string;
}

/** 基础数据 */
export const getBasicData = () => request.get<BasicDataItem[]>('/api/overview/basic_data');


export interface BucketListItem {
  id: string;
  name: string;
  acl: ACL;
  region: string;
  
  prevent_theft_url: {
    name: string;
    color: string;
  }[];
  created_time: string;
  file_count: number;
  size: number;
  state: string;
}


/**
 * 表格
 * @param params 
 */
export const getBucketList = (params: TableCommonParams) =>
  request.get<BucketListItem[]>('/api/overview/bucket_list', { params });

export interface BucketInfo {
  id?: string;
  name: string;
  region: string;
  acl: ACL;
}

/**
 * 新增/修改 Bucket
 * @param params 
 */
export const bucketSubmit = (params: BucketInfo) =>
  request.post('/api/overview/add_bucket', { data: params });

export interface StorageServiceListItem extends StorageServiceItem {
  id: string;
  /** 服务状态 */
  status: 'normal' | '...'; // TODO :
  /** 剩余空间 */
  remainderSpace: string;
  /** 总空间 */
  totalSpace: string;
}

/** 服务类型 分别为：主服务、备服务、归档服务 */
export type ServiceType = 'main' | 'spare' | 'archive';

/**
 * 获取服务列表
 * @returns 
 */
export const getServiceList = () =>
  request.get<Record<ServiceType, StorageServiceListItem[]>>('/api/overview/service_list');


export interface StorageServiceItem {
  id?: string;
  name: string;
  ip: string;
  port: string;
  storeLocation: string;
  acl: ACL;
  region: string;
}

/**
 * 添加 service
 */
export const addService = (data: StorageServiceItem) =>
  request.post<any>('/api/overview/service_add', { data });

interface ChartDataItem {
  date: string,
  num: number;
}

export type StatisticType = 'visit' | 'monthly';

/** 获取统计图表数据 */
export const getStatisticData = () =>
  request.get<Record<StatisticType, ChartDataItem[]>>('/api/overview/statistic_data');
