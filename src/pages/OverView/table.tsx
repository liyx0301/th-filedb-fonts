import React, { useRef, useState } from 'react';
import { Button } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import ProTable, { ProColumns, ActionType } from '@ant-design/pro-table';
import BucketDrawer from './bucket-drawer';
import { BucketInfo, BucketListItem, getBucketList } from './service';


const getColumns = (
  setInfo: (v: BucketInfo) => void,
  setVisible?: (v: boolean) => void,
): ProColumns<BucketListItem>[] => [
  {
    title: 'Bucket名称',
    dataIndex: 'name',
    ellipsis: true,
    tip: '标题过长会自动收缩',
    width: '30%',
    render: (text, row) => {
      return (
        <Button
          type="link"
          onClick={() => {
            setInfo({ id: row.id, name: row.name, acl: row.acl, region: row.region });
            setVisible?.(true);
          }}
        >
          {text}
        </Button>
      );
    },
  },
  {
    title: 'ACL控制',
    dataIndex: 'acl',
    initialValue: 'open',
    filters: true,
    onFilter: true,
    valueType: 'select',
    valueEnum: {
      PRIVATE: {
        text: '私有',
        status: 'PRIVATE',
      },
      PUBLIC_READ: {
        text: '公共读',
        status: 'PUBLIC_READ',
      },
      PUBLIC_READ_WRITE: {
        text: '公共读写',
        status: 'PUBLIC_READ_WRITE',
      },
    },
  },
  {
    title: '防盗链接',
    dataIndex: 'prevent_theft_url',
  },
  {
    title: '创建时间',
    dataIndex: 'created_time',
    valueType: 'date',
  },
  {
    title: '文件量',
    dataIndex: 'file_count',
    valueType: 'digit',
  },
  {
    title: '大小',
    dataIndex: 'size',
    valueType: 'digit',
  },
  {
    title: '状态',
    dataIndex: 'state',
  },
];

const Table = () => {
  const actionRef = useRef<ActionType>();
  const drawerRefNew = useRef<BucketDrawer>(null);
  const drawerRefMod = useRef<BucketDrawer>(null);
  const [info, setInfo] = useState<BucketInfo | undefined>(undefined);

  const columns = getColumns(setInfo, drawerRefMod.current?.changeVisible);

  const clearInfo = () => {
    setInfo(undefined);
  };

  return (
    <>
      <ProTable<BucketListItem>
        columns={columns}
        actionRef={actionRef}
        request={(params, sort, filter) => getBucketList({ params, sort, filter })}
        rowKey="id"
        search={false}
        options={{
          search: true,
        }}
        pagination={{
          pageSize: 5,
        }}
        dateFormatter="string"
        headerTitle="Bucket列表"
        toolBarRender={() => [
          <Button
            type="primary"
            onClick={() => {
              drawerRefNew.current?.changeVisible(true);
            }}
          >
            <PlusOutlined /> 新建
          </Button>
        ]}
      />
      <BucketDrawer ref={drawerRefNew} type="new" />
      <BucketDrawer ref={drawerRefMod} type="modify" info={info} clearInfo={clearInfo} />
    </>
  );
};

export default React.memo(Table);
