import React from 'react';
import { useIntl } from 'umi';
import { Space } from 'antd';
import ProCard from '@ant-design/pro-Card';
import BasicData from './basic-data';
import Table from './table';
import StorageServiceList from './storage-service-list';
import StaticCharts from './statistics-charts';

const Overview = () => {
  const intl = useIntl();

  return (
    <>
      <Space direction="vertical" size="small"  style={{ width: '100%' }}>
        <ProCard title="基础数据">
          <BasicData intl={intl} />
        </ProCard>

        <Table />
        
        <StorageServiceList />

        <StaticCharts intl={intl} />
      </Space>
    </>
  );
};

export default Overview;
