import React, { useState, useRef } from 'react';
import { Button, Space, Tag, message } from 'antd';
import { PlusOutlined } from "@ant-design/icons";
import ProList from '@ant-design/pro-list';
import ProCard from '@ant-design/pro-Card';
import { useRequest } from 'ahooks';
import ServerDrawer from '../AddServerDrawer/addServerDrawer';
import { StorageServiceListItem, StorageServiceItem, ServiceType, getServiceList } from '../service';

type DrawerType = 'new' | 'modify';

const STATUS_MAPPING = {
  normal: {
    text: '正常',
    color: '#f50',
  },
  // ...
};


const listConfig: {name: string;key: ServiceType}[] = [
  {
    name: '主存储服务',
    key: 'main',
  },
  {
    name: '备存储服务',
    key: 'spare',
  },
  {
    name: '归档存储服务',
    key: 'archive',
  },
];


const StorageServiceList = () => {

  const { data, loading } = useRequest(() => getServiceList(), {
    onError: (err) => {
      message.error(err.message);
    },
  });

  const drawerRefNew = useRef<ServerDrawer>(null);
  const drawerRefMod = useRef<ServerDrawer>(null);

  const [info, setInfo] = useState<StorageServiceItem | undefined>(undefined);

  const clearInfo = () => { setInfo(undefined) };

  return (
    <>
      <ProCard.Group loading={loading}>
        {listConfig.map((item) => {
          const dataSource = data?.[item.key].map((i: StorageServiceListItem) => ({
            ...i,
            desc: `剩余空间：${i.remainderSpace}——总空间：${i.totalSpace}`,
            tagText: STATUS_MAPPING[i.status].text,
            tagColor: STATUS_MAPPING[i.status].color,
          }));
          return (
            <ProCard key={item.key}>
              <ProList
                toolBarRender={() => item.key === 'archive' ? [
                  <Button
                    type="primary"
                    onClick={() => {
                      drawerRefNew.current?.changeVisible(true);
                    }}
                  >
                    <PlusOutlined /> 添加
                  </Button>
                ] : []}
                rowKey="name"
                headerTitle={item.name}
                dataSource={dataSource}
                showActions="hover"
                metas={{
                  title: {
                    dataIndex: 'name',
                    render: (text, row) => {
                      return (
                        <span
                          onClick={() => {
                            setInfo({
                              id: row.id,
                              ip: row.ip,
                              port: row.port,
                              name: row.name,
                              acl: row.acl,
                              region: row.region,
                              storeLocation: row.storeLocation,
                            });
                            drawerRefMod.current?.changeVisible(true);
                          }}
                        >
                          {text}
                        </span>
                      );
                    },
                  },
                  description: {
                    dataIndex: 'desc',
                  },
                  subTitle: {
                    render: (text, row) => {
                      return (
                        <Space size={0}>
                          <Tag color={row.tagColor}>{row.tagText}</Tag>
                        </Space>
                      );
                    },
                  },
                }}
              />
            </ProCard>
          );
        })}
      </ProCard.Group>
      <ServerDrawer ref={drawerRefNew} type="new" />
      <ServerDrawer ref={drawerRefMod} type="modify" info={info} clearInfo={clearInfo} />
    </>
  );
};

export default StorageServiceList;
