import React, { useRef } from "react";

import {
  Drawer,
  Form,
  Button,
  Radio,
  Row,
  Input,
  Select,
  DatePicker,
  message,
} from "antd";
import { FormInstance } from 'antd/lib/form';
import { ACL_Mapping, ACL } from '@/common-types/common';
import { StorageServiceItem, addService } from '../service';

import styles from "./index.less";


const { Option } = Select;
const ACLMessageTable: Record<ACL, React.ReactElement> = {
  PRIVATE:
  <span style={{ fontSize: '12px' }}>私有：对文件的所有访问操作需要进行身份验证。</span>,
  PUBLIC_READ:
  <span style={{ color: 'red', fontSize: '12px' }}>公共读：对文件写操作需要进行身份验证；可以对文件进行匿名读。</span>,
  PUBLIC_READ_WRITE:
  <span style={{ color: 'red', fontSize: '12px' }}>公共读写：所有人都可以对文件进行读写操作。</span>,
};
const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    s: { span: 5 },
    sm: { span: 4 },
  },
  wrapperCol: {
    xs: { span: 24 },
    s: { span: 19 },
    sm: { span: 20 },
  },
};
const TYPE_MAPPING: Record<Props['type'], string> = {
  new: '新建',
  modify: '编辑',
};

interface Props {
  type: 'new' | 'modify';
  info?: StorageServiceItem;
  clearInfo?: () => void;
}

interface State extends StorageServiceItem {
  visible: boolean;
  submitLoading: boolean;
  aclMessage: React.ReactElement;
  edit: boolean;
}

const defaultState: State = { 
  visible: false,
  edit: false,
  acl: 'PRIVATE',
  name: '',
  ip: '',
  port: '',
  storeLocation: '',
  submitLoading: false,
  aclMessage: ACLMessageTable.PRIVATE,
  region: 'oss-shanghai-1', // TODO :
};

class DrawerForm extends React.Component<Props, State> {
  formRef = React.createRef<FormInstance>();

  constructor(props: Props) {
    super(props);
    this.state = { ...defaultState, edit: props.type === 'new' };
  }

  changeVisible = (v: boolean) => {
    if (!v) {
      this.props.clearInfo?.();
      if (this.props.type === 'modify') this.setState({ edit: false });
    }
    this.setState({ visible: v });
  };

  createBucketSubmit = (values: StorageServiceItem) => {
    const { info, type } = this.props;
    this.setState({
      submitLoading: true,
    });
    const params = {
      ...values,
      id: type === 'modify' ? info?.id : undefined,
    };
    addService(params)
      .then(() => {
        message.success(`成功${TYPE_MAPPING[type]} Service`);
        this.setState({ visible: false });
      })
      .catch((err) => {
        message.error(err.message);
      })
      .finally(() => {
        this.setState({
          submitLoading: false,
        });
      });
  };

  aclChange = (e: any) => {
    e.preventDefault();
    this.setState({
      aclMessage: ACLMessageTable[e.target.value],
    });
  };

  checkBucketName = (rule: any, value: any) => {
    if (!value || value.trim().length < 3 || value.trim().length > 63) {
      return Promise.reject(<span style={{ fontSize: '12px' }}>请输入 Server 名称，3-63个字符长度</span>);
    }
    if (!/^[A-Za-z0-9][A-Za-z0-9-]{1,61}[A-Za-z0-9]$/.test(value)) {
      return Promise.reject(<span style={{ fontSize: '12px' }}>只允许字母、数字、中划线（-），且不能以短横线开头或结尾</span>);
    }
    this.setState({
      name: value,
    });
    return Promise.resolve();
  };
  render() {
    const { type, info } = this.props;
    const { edit, name, submitLoading, aclMessage } = this.state;

    const buttons = edit ? 
      <>
        <Button onClick={() => this.changeVisible(false)} style={{ marginRight: 8 }}>
          取消
        </Button>
        <Button
          loading={submitLoading}
          type="primary"
          htmlType="submit"
          style={{
            marginRight: 8,
          }}
          disabled={!name}
          onClick={() => {this.formRef.current?.submit()}}
        >
          确认
        </Button>
      </>
    : <Button type="primary" onClick={() => this.setState({ edit: true })}>编辑</Button>;

    const initialFormValue = info ?? {
      name: '',
      ip: defaultState.ip,
      port: defaultState.port,
      storeLocation: defaultState.storeLocation,
      acl: defaultState.acl,
      region: defaultState.region,
    };

    return (
      <div className={styles.container}>
        <div id="components-drawer-demo-form-in-drawer">
          
          <Drawer
            title={`${TYPE_MAPPING[type]} Server`}
            width={640}
            onClose={() => this.changeVisible(false)}
            visible={this.state.visible}
            bodyStyle={{ paddingBottom: 80 }}
            destroyOnClose
            footer={
              <div
                style={{
                  textAlign: "right"
                }}
              >
                {buttons}
              </div>
            }
          >
            <Form {...formItemLayout} ref={this.formRef} onFinish={this.createBucketSubmit} hideRequiredMark initialValues={initialFormValue}>
            
              <Form.Item
                name="name"
                label="Server 名称"
                extra={<span style={{ fontSize: '12px' }}>Server 名称，3-63 个字符</span>}
                rules={[{ required: true },
                  { validator: this.checkBucketName }]}
              >
                <Input disabled={!edit} placeholder="Bucket" suffix={`${name.length}/63`} />
              </Form.Item>

              <Form.Item
                name="ip"
                label="IP地址"
                extra={<span style={{ fontSize: '12px' }}>Server 名称，3-63 个字符</span>}
                rules={[{ required: true },
                  { validator: this.checkBucketName }]}
              >
                <Input disabled={!edit} placeholder="Bucket" suffix={`${name.length}/63`} />
              </Form.Item>

              <Form.Item
                name="port"
                label="端口"
                extra={<span style={{ fontSize: '12px' }}>Server 名称，3-63 个字符</span>}
                rules={[{ required: true },
                  { validator: this.checkBucketName }]}
              >
                <Input disabled={!edit} placeholder="Bucket" suffix={`${name.length}/63`} />
              </Form.Item>

              <Form.Item
                name="storeLocation"
                label="存储位置"
                extra={<span style={{ fontSize: '12px' }}>Server 名称，3-63 个字符</span>}
                rules={[{ required: true },
                  { validator: this.checkBucketName }]}
              >
                <Input disabled={!edit} placeholder="Bucket" suffix={`${name.length}/63`} />
              </Form.Item>

              <Form.Item
                name="acl"
                label="存储类型"
                validateStatus="success"
                help={aclMessage}
                rules={[{
                  required: true,
                  message: '请选择存储类型!',
                }]}
              >
                <Radio.Group disabled={!edit} onChange={this.aclChange} buttonStyle="solid">
                  {Object.entries(ACL_Mapping).map(([key, value]) => (
                    <Radio.Button value={key}>{value}</Radio.Button>
                  ))}
                </Radio.Group>
              </Form.Item>
            </Form>
          </Drawer>
        </div>
      </div>
    );
  }
}

export default DrawerForm;
