import React from 'react';
import { IntlShape } from 'react-intl';
import { useRequest } from 'ahooks';
import { Alert, message } from 'antd';
import ProCard from '@ant-design/pro-Card';
import { Line } from '@ant-design/charts';
import { getStatisticData } from './service';

interface Props {
  intl: IntlShape;
}

const StaticsCharts = (props: Props) => {

  const { data, loading } = useRequest(() => getStatisticData(), {
    onError: (err) => {
      message.error(err.message);
    },
  });

  const config = {
    xField: 'date',
    yField: 'num',
    autoFit: false,
    point: {
      size: 5,
      shape: 'diamond',
      style: {
        fill: 'white',
        stroke: '#5B8FF9',
        lineWidth: 2,
      },
    },
    tooltip: {
      formatter: (datum: DataCue) => {
        return { name: '新增', value:`${datum.num}个` };
      },
    },
    state: {
      active: {
        style: {
          shadowColor: 'yellow',
          shadowBlur: 4,
          stroke: 'transparent',
          fill: 'red',
        },
      },
    },
    theme: {
      geometries: {
        point: {
          diamond: {
            active: {
              style: {
                shadowColor: '#FCEBB9',
                shadowBlur: 2,
                stroke: '#F6BD16',
              },
            },
          },
        },
      },
    },
    interactions: [{ type: 'marker-active' }],
  };

  return (
    <ProCard.Group loading={loading}>
      <ProCard title="文件月度统计" style={{ width: '100%' }}>
        <Alert
            message={props.intl.formatMessage({
              id: '统计12个月的文件数据量。',
              defaultMessage: '统计12个月的文件数据量。',
            })}
            type="success"
            showIcon
            banner
            style={{
              margin: -20,
              marginBottom: 1,
            }}
          />
        <Line height={300} {...config} data={data?.monthly || []} />
      </ProCard>

      <ProCard title="文件访问统计" style={{ width: '100%' }}>
        <Alert
          message={props.intl.formatMessage({
            id: '统计12个月的文件访问次数。',
            defaultMessage: '统计12个月的文件访问次数。',
          })}
          type="success"
          showIcon
          banner
          style={{
            margin: -20,
            marginBottom: 1,
          }}
        />
        <Line height={300} {...config} data={data?.visit || []} />
      </ProCard>
    </ProCard.Group>
  );
};

export default StaticsCharts;
