import React, { useRef } from 'react';
import { Button, Tag, Space, Menu, Dropdown } from 'antd';
import type { ProColumns, ActionType } from '@ant-design/pro-table';
import ProTable, { TableDropdown } from '@ant-design/pro-table';
import request from '@/utils/request';
import { PageContainer } from '@ant-design/pro-layout';

type GithubIssueItem = {
  url: string;
  id: number;
  number: number;
  title: string;
  labels: {
    name: string;
    color: string;
  }[];
  state: string;
  comments: number;
  created_at: string;
  updated_at: string;
  closed_at?: string;
};

const columns: ProColumns<GithubIssueItem>[] = [
  {
    title: '标题',
    dataIndex: 'title',
    copyable: true,
    ellipsis: true,
    formItemProps: {
      rules: [
        {
          required: true,
          message: '此项为必填项',
        },
      ],
    },
    width: '30%',
  },
  {
    title: '状态',
    dataIndex: 'state',
    initialValue: 'open',
    filters: true,
    onFilter: true,
    valueType: 'select',
    valueEnum: {
      all: { text: '全部', status: 'Default' },
      open: {
        text: '未解决',
        status: 'Error',
      },
      closed: {
        text: '已解决',
        status: 'Success',
        disabled: true,
      },
      processing: {
        text: '解决中',
        status: 'Processing',
      },
    },
  },
  {
    title: '标签',
    dataIndex: 'labels',
    renderFormItem: (_, { defaultRender }) => {
      return defaultRender(_);
    },
    render: (_, record) => (
      <Space>
        {record.labels.map(({ name, color }) => (
          <Tag color={color} key={name}>
            {name}
          </Tag>
        ))}
      </Space>
    ),
  },
  {
    title: '创建时间',
    key: 'created_at',
    dataIndex: 'created_at',
    valueType: 'date',
  },
  {
    title: '操作',
    valueType: 'option',
    render: (text, record, _, action) => [
      <a href={record.url} target="_blank" rel="noopener noreferrer" key="view">
        详情
      </a>,
      <a href={record.url} target="_blank" rel="noopener noreferrer" key="view">
        下载
      </a>,
      <a href={record.url} target="_blank" rel="noopener noreferrer" key="view">
        设置权限
      </a>,

      <TableDropdown
        key="actionGroup"
        onSelect={() => action.reload()}
        menus={[
          { key: 'copyUrl', name: '复制URL' },
          { key: 'delete', name: '删除' },
        ]}
      ></TableDropdown>,
    ],
  },
];

const menu = (
  <Menu>
    <Menu.Item key="1">删除</Menu.Item>
    <Menu.Item key="2">下载</Menu.Item>
  </Menu>
);

export default () => {
  const actionRef = useRef<ActionType>();

  return (
    <PageContainer>
      <ProTable<GithubIssueItem>
        columns={columns}
        actionRef={actionRef}
        request={async (params = {}) =>
          request<{
            data: GithubIssueItem[];
          }>('https://proapi.azurewebsites.net/github/issues', {
            params,
          })
        }
        editable={{
          type: 'multiple',
        }}
        bordered={true}
        rowKey="id"
        rowSelection={<></>}
        search={false}
        options={{
          search: true,
        }}
        pagination={{
          pageSize: 5,
        }}
        dateFormatter="string"
        headerTitle={
          <Space>
            <Dropdown key="menu" overlay={menu}>
              <Button>批量操作</Button>
            </Dropdown>
          </Space>
        }
      />
    </PageContainer>
  );
};
