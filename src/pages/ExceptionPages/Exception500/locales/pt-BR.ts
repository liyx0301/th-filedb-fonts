export default {
  'exceptionpagesandexception500.exception.back': 'Voltar para Início',
  'exceptionpagesandexception500.description.500': 'Desculpe, o servidor está reportando um erro.',
};
