export default {
  'exceptionpagesandexception500.exception.back': 'Back to home',
  'exceptionpagesandexception500.description.500': 'Sorry, the server is reporting an error.',
};
