import request from '@/utils/request';
import { TableCommonParams } from '@/common-types/common';

export interface UserListItem {
  id: number;
  username: string;
  email: string;
  nick_name: string;
  phoneNum: number;
  activated: string;
  enabled: string;
  locked: string;
  expired: string;
  create_time: Date;
  update_time: Date;
};

export const getUserList = (params: TableCommonParams) =>
  request.get<UserListItem[]>('/api/usermanager/user_list', { params });
