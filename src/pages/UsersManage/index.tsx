import React, { useRef } from 'react';
import type { ProColumns, ActionType } from '@ant-design/pro-table';
import { Button, Space } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import ProTable from '@ant-design/pro-table';
import { PageContainer } from '@ant-design/pro-layout';
import { UserListItem,getUserList } from './service';
import AddUserDrawer from './AddUserDrawer/index';

const columns: ProColumns<UserListItem>[] = [
  {
    title: '账号',
    key: 'username',
    dataIndex: 'username',
  },
  {
    title: '用户名',
    key: 'nick_name',
    dataIndex: 'nick_name',
  },
  {
    title: '手机号',
    key: 'phoneNum',
    dataIndex: 'phoneNum',
  },
  {
    title: '邮箱',
    key: 'email',
    dataIndex: 'email',
  },
  {
    title: '激活状态',
    key: 'activated',
    dataIndex: 'activated',
  },
  {
    title: '启用状态',
    key: 'enabled',
    dataIndex: 'enabled',
  },
  {
    title: '是否锁定',
    key: 'locked',
    dataIndex: 'locked',
  },
  {
    title: '过期时间',
    key: 'expired',
    dataIndex: 'expired',
    valueType: 'date',
  },
  {
    title: '创建时间',
    key: 'create_time',
    dataIndex: 'create_time',
    valueType: 'date',
  },
  {
    title: '更新时间',
    key: 'update_time',
    dataIndex: 'update_time',
    valueType: 'date',
  },
];

export default () => {
  const actionRef = useRef<ActionType>();

  return (
    <PageContainer>
      <ProTable<UserListItem>
        columns={columns}
        actionRef={actionRef}
        request={(params) => getUserList(params)}
        bordered={true}
        rowKey="id"
        rowSelection={<></>}
        search={false}
        options={{
          search: true,
        }}
        pagination={{
          pageSize: 5,
        }}
        dateFormatter="string"
        headerTitle={
          <Space>
            <Button
              type="primary"
            > 批量激活
            </Button>
            <Button
              type="primary"
            > 批量禁用
            </Button>
            <Button
              type="primary"
            > 批量启用
            </Button>
            <Button
              type="primary"
            > 批量锁定
            </Button>
            <Button
              type="primary"
            > 批量解锁
            </Button>
          </Space>
        }
        toolBarRender={() => [
          <AddUserDrawer />,
        ]}
      />
      
    </PageContainer>
  );
};
