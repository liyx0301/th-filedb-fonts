import { PageContainer } from '@ant-design/pro-layout';
import React, { useState, useEffect } from 'react';
import ProfileBasic from './ProfileBasic';

export default () => {
  const [loading, setLoading] = useState<boolean>(true);
  useEffect(() => {
    setTimeout(() => {
      setLoading(false);
    }, 3000);
  }, []);
  return (
    <PageContainer>
      <ProfileBasic />
    </PageContainer>
  );
};
