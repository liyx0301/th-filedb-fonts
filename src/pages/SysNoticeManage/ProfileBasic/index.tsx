import { Badge, Card, Descriptions, Divider, Table } from 'antd';
import React, { Component } from 'react';

import { PageContainer } from '@ant-design/pro-layout';
import { connect, Dispatch } from 'umi';
import { BasicProfileDataType } from './data.d';
import styles from './style.less';

const progressColumns = [
  {
    title: '时间',
    dataIndex: 'time',
    key: 'time',
  },
  {
    title: '当前进度',
    dataIndex: 'rate',
    key: 'rate',
  },
  {
    title: '状态',
    dataIndex: 'status',
    key: 'status',
    render: (text: string) => {
      if (text === 'success') {
        return <Badge status="success" text="成功" />;
      }
      return <Badge status="processing" text="进行中" />;
    },
  },

  {
    title: '操作员ID',
    dataIndex: 'operator',
    key: 'operator',
  },
  {
    title: '耗时',
    dataIndex: 'cost',
    key: 'cost',
  },
];

interface ProfileBasicProps {
  loading: boolean;
  dispatch: Dispatch<any>;
  sysNoticeManageAndProfileBasic: BasicProfileDataType;
}
interface ProfileBasicState {
  visible: boolean;
}

class ProfileBasic extends Component<
  ProfileBasicProps,
  ProfileBasicState
> {
  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'sysNoticeManageAndProfileBasic/fetchBasic',
    });
  }

  render() {
    const { sysNoticeManageAndProfileBasic, loading } = this.props;
    const { basicGoods, basicProgress } = sysNoticeManageAndProfileBasic;
    let goodsData: typeof basicGoods = [];
    if (basicGoods.length) {
      let num = 0;
      let amount = 0;
      basicGoods.forEach((item) => {
        num += Number(item.num);
        amount += Number(item.amount);
      });
      goodsData = basicGoods.concat({
        id: '总计',
        num,
        amount,
      });
    }
    const renderContent = (value: any, row: any, index: any) => {
      const obj: {
        children: any;
        props: { colSpan?: number };
      } = {
        children: value,
        props: {},
      };
      if (index === basicGoods.length) {
        obj.props.colSpan = 0;
      }
      return obj;
    };
    const goodsColumns = [
      {
        title: '商品编号',
        dataIndex: 'id',
        key: 'id',
        render: (text: React.ReactNode, row: any, index: number) => {
          if (index < basicGoods.length) {
            return <a href="">{text}</a>;
          }
          return {
            children: <span style={{ fontWeight: 600 }}>总计</span>,
            props: {
              colSpan: 4,
            },
          };
        },
      },
      {
        title: '商品名称',
        dataIndex: 'name',
        key: 'name',
        render: renderContent,
      },
      {
        title: '商品条码',
        dataIndex: 'barcode',
        key: 'barcode',
        render: renderContent,
      },
      {
        title: '单价',
        dataIndex: 'price',
        key: 'price',
        align: 'right' as 'left' | 'right' | 'center',
        render: renderContent,
      },
      {
        title: '数量（件）',
        dataIndex: 'num',
        key: 'num',
        align: 'right' as 'left' | 'right' | 'center',
        render: (text: React.ReactNode, row: any, index: number) => {
          if (index < basicGoods.length) {
            return text;
          }
          return <span style={{ fontWeight: 600 }}>{text}</span>;
        },
      },
      {
        title: '金额',
        dataIndex: 'amount',
        key: 'amount',
        align: 'right' as 'left' | 'right' | 'center',
        render: (text: React.ReactNode, row: any, index: number) => {
          if (index < basicGoods.length) {
            return text;
          }
          return <span style={{ fontWeight: 600 }}>{text}</span>;
        },
      },
    ];
    return (
        <Card bordered={false}>
          <Descriptions title="短信设置" style={{ marginBottom: 32 }}>
            <Descriptions.Item label="手机号">13900000000</Descriptions.Item>
            <Descriptions.Item label="状态">已开启</Descriptions.Item>
          </Descriptions>
          <Divider style={{ marginBottom: 32 }} />
          <Descriptions title="邮件设置" style={{ marginBottom: 32 }}>
            <Descriptions.Item label="邮箱地址：">13900000000@163.com</Descriptions.Item>
            <Descriptions.Item label="状态">已开启</Descriptions.Item>
          </Descriptions>
        </Card>
    );
  }
}

export default connect(
  ({
    sysNoticeManageAndProfileBasic,
    loading,
  }: {
    sysNoticeManageAndProfileBasic: BasicProfileDataType;
    loading: {
      effects: { [key: string]: boolean };
    };
  }) => ({
    sysNoticeManageAndProfileBasic,
    loading: loading.effects['sysNoticeManageAndProfileBasic/fetchBasic'],
  }),
)(ProfileBasic);
