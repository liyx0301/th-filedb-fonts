import request from '@/utils/request';
const server_host = '';
/**
 *  获取 Bucket 列表
 * @returns {Promise<AxiosResponse<any> | never>}
 * @constructor
 */
export async function ListBucketApi(): Promise<any>{
  return request(server_host+'/api/bucket/list.json');
}

/**
 * 获取 Bucket 详情
 * @param params
 * @returns {Promise<AxiosResponse<any> | never>}
 * @constructor
 */
export async function GetBucketApi(params:String) : Promise<any>{
  return request(server_host+'/api/bucket/detail.json');
}

/**
 * 获取 Bucket 防盗链设置
 * @param params
 * @returns {Promise<AxiosResponse<any> | never>}
 * @constructor
 */
export async function GetBucketRefererApi(params:String) : Promise<any>{
  return request(server_host+'/api/bucket/get_referer.json');
}
/**
 * 更新 防盗链设置
 * @param params
 * @returns {Promise<AxiosResponse<T>>}
 * @constructor
 */
export async function UpdateRefererApi(params:String) : Promise<any>{
  return request(server_host+'/api/bucket/update_referer.json');
}
/**
 * 创建 Bucket
 * @returns {Promise<AxiosResponse<any> | never>}
 * @constructor
 */
export async function CreateBucketApi(params:String) : Promise<any>{
  return request(server_host+'/api/bucket/new_create_bucket.json');
}

/**
 * 设置 Bucket ACL
 * @param params
 * @returns {Promise<AxiosResponse<any> | never>}
 * @constructor
 */
export async function SetBucketAclApi(params:String) : Promise<any>{
  return request(server_host+'/api/bucket/set_acl.json');
}
/**
 * 删除 Bucket
 * @param params
 * @returns {Promise<AxiosResponse<any> | never>}
 * @constructor
 */
export async function UpdateBucketApi(params:String) : Promise<any>{
  return request(server_host+'/api/bucket/update_bucket.json');
}
/**
 * 删除 Bucket
 * @param params
 * @returns {Promise<AxiosResponse<any> | never>}
 * @constructor
 */
export async function DeleteBucketApi(params:String) : Promise<any>{
  return request(server_host+'/api/bucket/delete_bucket.json');
}