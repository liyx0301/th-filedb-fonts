const AclTable = {
  EXTEND_BUCKET: '继承 Bucket',
  PRIVATE: '私有',
  PUBLIC_READ: '公共读',
  PUBLIC_READ_WRITE: '公共读写',
};

export function getAclDesc(acl: string) {
  return AclTable[acl];
}
