import { Request, Response } from 'express';

// 代码中会兼容本地 service mock 以及部署站点的静态数据
export default {
  // 支持值为 Object 和 Array
  'GET /api/bucket/list.json': {
    name: '李顾怀',
    avatar: 'https://gw.alipayobjects.com/zos/antfincdn/XAosXuNZyF/BiazfanxmamNRoxxVxka.png',
    userid: '00000001',
    email: 'antdesign@alipay.com',
  },
  // GET POST 可省略
  'GET /api/bucket/detail11': [
    {
      key: '1',
      name: 'John Brown',
      age: 32,
      address: 'New York No. 1 Lake Park',
    },
  ],
  
  'POST /api/bucket/update_referer.json': (req: Request, res: Response) => {
    res.send({ status: 'ok', currentAuthority: 'user' });
  },

  'GET /api/overview/basic_data': [
    { title: '存储空间', value: 119.0, precision: 1, suffix: 'T' },
    { title: '已用空间', value: 11.0, precision: 1, suffix: 'T' },
    { title: '剩余空间', value: 113.0, precision: 1, suffix: 'T' },
    { title: 'Bucket数量', value: 1193 },
    { title: 'Object数量', value: 989437973 },
    { title: '最大Object大小', value: 982, suffix: 'M' },
    { title: '最小Object大小', value: 98, suffix: 'K' },
    { title: '平均Object大小', value: 912, suffix: 'M' },
  ],

  'GET /api/overview/bucket_list': {
    data: [
      {
        id: 1,
        name: 'bucket-1',
        acl: 'PRIVATE',
        prevent_theft_url: '未开启',
        created_time: '2021-03-12',
        file_count: 10,
        size: 15,
        state: '启用',
        region: '华东1 上海',
      },
    ],
    success: true,
    total: 2,
  },

  'POST /api/overview/add_bucket': {},

  'GET /api/overview/service_list': {
    main: [
      {
        name: 'Server01',
        remainderSpace: '1929G',
        totalSpace: '19383G',
        status: 'normal',
        acl: 'PRIVATE',
        ip: '124.159.52.4',
        port: '5482',
        region: 'sdfsfsdf',
      },
      {
        name: 'Server02',
        remainderSpace: '1929G',
        totalSpace: '19383G',
        status: 'normal',
        acl: 'PRIVATE',
        ip: '124.159.52.4',
        port: '5482',
        region: 'sdfsfsdf',
      },
      {
        name: 'Server03',
        remainderSpace: '1929G',
        totalSpace: '19383G',
        status: 'normal',
        acl: 'PRIVATE',
        ip: '124.159.52.4',
        port: '5482',
        region: 'sdfsfsdf',
      },
      {
        name: 'Server04',
        remainderSpace: '1929G',
        totalSpace: '19383G',
        status: 'normal',
        acl: 'PRIVATE',
        ip: '124.159.52.4',
        port: '5482',
        region: 'sdfsfsdf',
      },
      {
        name: 'Server05',
        remainderSpace: '1929G',
        totalSpace: '19383G',
        status: 'normal',
        acl: 'PRIVATE',
        ip: '124.159.52.4',
        port: '5482',
        region: 'sdfsfsdf',
      },
    ],
    spare: [
      {
        name: 'Server01',
        remainderSpace: '1929G',
        totalSpace: '19383G',
        status: 'normal',
        acl: 'PRIVATE',
        ip: '124.159.52.4',
        port: '5482',
        region: 'sdfsfsdf',
      },
      {
        name: 'Server02',
        remainderSpace: '1929G',
        totalSpace: '19383G',
        status: 'normal',
        acl: 'PRIVATE',
        ip: '124.159.52.4',
        port: '5482',
        region: 'sdfsfsdf',
      },
      {
        name: 'Server03',
        remainderSpace: '1929G',
        totalSpace: '19383G',
        status: 'normal',
        acl: 'PRIVATE',
        ip: '124.159.52.4',
        port: '5482',
        region: 'sdfsfsdf',
      },
      {
        name: 'Server04',
        remainderSpace: '1929G',
        totalSpace: '19383G',
        status: 'normal',
        acl: 'PRIVATE',
        ip: '124.159.52.4',
        port: '5482',
        region: 'sdfsfsdf',
      },
      {
        name: 'Server05',
        remainderSpace: '1929G',
        totalSpace: '19383G',
        status: 'normal',
        acl: 'PRIVATE',
        ip: '124.159.52.4',
        port: '5482',
        region: 'sdfsfsdf',
      },
    ],
    archive: [
      {
        name: 'Server01',
        remainderSpace: '1929G',
        totalSpace: '19383G',
        status: 'normal',
        acl: 'PRIVATE',
        ip: '124.159.52.4',
        port: '5482',
        region: 'sdfsfsdf',
      },
      {
        name: 'Server02',
        remainderSpace: '1929G',
        totalSpace: '19383G',
        status: 'normal',
        acl: 'PRIVATE',
        ip: '124.159.52.4',
        port: '5482',
        region: 'sdfsfsdf',
      },
      {
        name: 'Server03',
        remainderSpace: '1929G',
        totalSpace: '19383G',
        status: 'normal',
        acl: 'PRIVATE',
        ip: '124.159.52.4',
        port: '5482',
        region: 'sdfsfsdf',
      },
      {
        name: 'Server04',
        remainderSpace: '1929G',
        totalSpace: '19383G',
        status: 'normal',
        acl: 'PRIVATE',
        ip: '124.159.52.4',
        port: '5482',
        region: 'sdfsfsdf',
      },
      {
        name: 'Server05',
        remainderSpace: '1929G',
        totalSpace: '19383G',
        status: 'normal',
        acl: 'PRIVATE',
        ip: '124.159.52.4',
        port: '5482',
        region: 'sdfsfsdf',
      },
    ],
  },

  'POST /api/overview/service_add': {},

  'GET /api/overview/statistic_data': {
    visit: [
      { date: '2020-01', num: 38674 },
      { date: '2020-02', num: 49474 },
      { date: '2020-03', num: 378474 },
      { date: '2020-04', num: 58475 },
      { date: '2020-05', num: 474367 },
      { date: '2020-06', num: 61723 },
      { date: '2020-07', num: 74562 },
      { date: '2020-08', num: 908478 },
      { date: '2020-09', num: 937437 },
      { date: '2020-10', num: 12223 },
      { date: '2020-11', num: 184743 },
      { date: '2020-12', num: 13987 },
    ],
    monthly: [
      { date: '2020-01', num: 38674 },
      { date: '2020-02', num: 49474 },
      { date: '2020-03', num: 378474 },
      { date: '2020-04', num: 58475 },
      { date: '2020-05', num: 474367 },
      { date: '2020-06', num: 61723 },
      { date: '2020-07', num: 74562 },
      { date: '2020-08', num: 908478 },
      { date: '2020-09', num: 937437 },
      { date: '2020-10', num: 12223 },
      { date: '2020-11', num: 184743 },
      { date: '2020-12', num: 13987 },
    ],
  },

  'GET /api/bucket_manage/datail_basic': {
    create_time: '2021-01-01 12:58:34',
    file_count: 484547748,
    size_count: '1282G',
    acl: 'PRIVATE',
    prevent_theft_url: '',
  },

  'GET /api/bucket_manage/datail_chart': {
    add: [
      {
        date: '2020-01',
        num: 38674,
      },
      {
        date: '2020-02',
        num: 49474,
      },
      {
        date: '2020-03',
        num: 378474,
      },
      {
        date: '2020-04',
        num: 58475,
      },
      {
        date: '2020-05',
        num: 474367,
      },
      {
        date: '2020-06',
        num: 61723,
      },
      {
        date: '2020-07',
        num: 74562,
      },
      {
        date: '2020-08',
        num: 908478,
      },
      {
        date: '2020-09',
        num: 937437,
      },
      {
        date: '2020-10',
        num: 12223,
      },
      {
        date: '2020-11',
        num: 184743,
      },
      {
        date: '2020-12',
        num: 13987,
      },
    ],
    quote: [
      {
        date: '2020-01',
        num: 38674,
      },
      {
        date: '2020-02',
        num: 49474,
      },
      {
        date: '2020-03',
        num: 378474,
      },
      {
        date: '2020-04',
        num: 58475,
      },
      {
        date: '2020-05',
        num: 474367,
      },
      {
        date: '2020-06',
        num: 61723,
      },
      {
        date: '2020-07',
        num: 74562,
      },
      {
        date: '2020-08',
        num: 908478,
      },
      {
        date: '2020-09',
        num: 937437,
      },
      {
        date: '2020-10',
        num: 12223,
      },
      {
        date: '2020-11',
        num: 184743,
      },
      {
        date: '2020-12',
        num: 13987,
      },
    ],
  },

  'GET /api/bucket_manage/hot_data': [
    {
      date: '2020-01',
      num: 38674,
    },
    {
      date: '2020-02',
      num: 49474,
    },
    {
      date: '2020-03',
      num: 378474,
    },
    {
      date: '2020-04',
      num: 58475,
    },
    {
      date: '2020-05',
      num: 474367,
    },
    {
      date: '2020-06',
      num: 61723,
    },
    {
      date: '2020-07',
      num: 74562,
    },
    {
      date: '2020-08',
      num: 908478,
    },
    {
      date: '2020-09',
      num: 937437,
    },
    {
      date: '2020-10',
      num: 12223,
    },
    {
      date: '2020-11',
      num: 184743,
    },
    {
      date: '2020-12',
      num: 13987,
    },
  ],

  'GET /api/bucket_manage/file_manage': {
    data: [
      {
        id: '1',
        title: '文件-111',
        state: 'Success',
        labels: [{ name: 'abc', color: 'green' }],
        created_time: '2020-11-23',
      },
      {
        id: '2',
        title: '文件-222',
        state: 'Error',
        labels: [{ name: 'abc', color: 'green' }],
        created_time: '2020-11-25',
      },
    ],
    success: true,
    total: 2,
  },

  'POST /api/bucket_manage/upload_file': {},

  'POST /api/bucket_manage/add_folder': {},

  'POST /api/bucket_manage/files_operate': {},

  'GET /api/bucket_manage/detail': {
    acl: 'acl',
    name: 'name',
    referer: true,
  },

  'POST /api/bucket_manage/set_acl': { msg: 'SUCCESS' },

  'GET /api/bucket_manage/referer': { msg: 'SUCCESS' },

  'POST /api/bucket_manage/update_referer': { msg: 'SUCCESS' },

  'POST /api/bucket_manage/delete_bucket': { msg: 'SUCCESS' },

  'GET /api/global/routes': ['ERP-Buckend', 'OA-Buckend', 'HR-Buckend', 'DOC-Buckend'],
};
