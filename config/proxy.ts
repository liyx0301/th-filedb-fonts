/**
 * 在生产环境 代理是无法生效的，所以这里没有生产环境的配置
 */
export default {
  dev: {
    '/api': {
      target: 'http://127.0.0.1:8081',
      changeOrigin: true,
      pathRewrite: { '/api': '' },
    },
  },
  test: {
    '/api/': {
      target: 'http://127.0.0.1:8081',
      changeOrigin: true,
      pathRewrite: { '^': '' },
    },
  },
  pre: {
    '/api/': {
      target: 'your pre url',
      changeOrigin: true,
      pathRewrite: { '^': '' },
    },
  },
};
