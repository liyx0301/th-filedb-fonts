export default [
  {
    path: '/',
    component: '../layouts/BlankLayout',
    routes: [
      {
        path: '/user',
        component: '../layouts/UserLayout',
        routes: [
          {
            name: 'login',
            path: '/user/login',
            component: './User/UserLogin/index',
          },
        ],
      },
      {
        path: '/',
        component: '../layouts/SecurityLayout',
        routes: [
          {
            path: '/',
            component: '../layouts/BasicLayout',
            authority: ['admin', 'user'],
            routes: [
              {
                path: '/',
                redirect: '/overview',
              },
              {
                path: '/overview',
                name: '概览',
                icon: 'FundProjectionScreenOutlined',
                component: './Overview/index',
              },
              {
                path: '/buckend',
                name: 'Bucket管理',
                icon: 'LaptopOutlined',
                authority: ['admin'],
                routes: [
                  {
                    path: '/buckend/ERP-Buckend',
                    name: 'ERP-Buckend',
                    icon: 'smile',
                    component: './BucketManage/index',
                    authority: ['admin'],
                  },
                  {
                    path: '/buckend/OA-Buckend',
                    name: 'OA-Buckend',
                    icon: 'smile',
                    component: './BucketManage/index',
                    authority: ['admin'],
                  },
                  {
                    path: '/buckend/HR-Buckend',
                    name: 'HR-Buckend',
                    icon: 'smile',
                    component: './BucketManage/index',
                    authority: ['admin'],
                  },
                  {
                    path: '/buckend/DOC-Buckend',
                    name: 'DOC-Buckend',
                    icon: 'smile',
                    component: './BucketManage/index',
                    authority: ['admin'],
                  },
                ],
              },
              {
                path: '/Server',
                name: '存储服务管理',
                icon: 'DatabaseOutlined',
                component: './FileServerManage/index',
                authority: ['admin'],
              },
              {
                name: '密钥管理',
                icon: 'Key',
                path: '/mygs',
                component: './SecretKeyManage/index',
                authority: ['admin'],
              },
              {
                name: '用户管理',
                icon: 'ApartmentOutlined',
                path: '/yhgl',
                component: './UsersManage/index',
                authority: ['admin'],
              },
              {
                name: '系统日志',
                icon: 'SnippetsOutlined',
                path: './xtrj',
                component: './SysLogManage/index',
                authority: ['admin'],
              },
              {
                name: '通知管理',
                icon: 'MessageOutlined',
                path: '/tzgl',
                component: './SysNoticeManage/index',
                authority: ['admin'],
              },
              {
                name: '角色管理',
                icon: 'TeamOutlined',
                path: '/jsgl',
                component: './RoleManage/index',
                authority: ['admin'],
              },
              {
                name: '跨区域同步',
                icon: 'ControlOutlined',
                path: '/kqytb',
                component: './RoleManage/index',
                authority: ['admin'],
              },
              {
                name: '数据迁移',
                icon: 'SwapOutlined',
                path: '/sjqy',
                component: './RoleManage/index',
                authority: ['admin'],
              },
              {
                name: '帮助文档',
                icon: 'FileSearchOutlined',
                path: '/bzwd',
                component: './RoleManage/index',
                authority: ['admin'],
              },
              {
                name: '开发指南',
                icon: 'HighlightOutlined',
                path: '/kfazn',
                component: './RoleManage/index',
                authority: ['admin'],
              },
              {
                component: './404',
              },
            ],
          },
        ],
      },
    ],
  },
  {
    component: './404',
  },
];
